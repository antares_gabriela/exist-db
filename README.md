Proyecto Maven en Eclipse para probar la conectividad de una base de datos Exist con código Java (NO COMPLETADO)

### Tutorial para la instalación Plugin Exist-db en Eclipse ###

* https://okule.wordpress.com/2012/06/24/the-most-powerful-tool-to-make-full-use-of-xquery/

### Tutoriales útiles para finalizar el proyecto ###

* Originales de la pàgina de Exist: http://exist-db.org/exist/apps/homepage/index.html
* Otros: https://gim.unex.es/blogs/ljarevalo/2009/03/08/java-exist-un-par-inseparable/